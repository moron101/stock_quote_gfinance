import tkinter,sys, re
import StockQuoter              # My class
from tkinter import ttk
from tkinter import messagebox

root_win_closed = False

def mouseBusy():
    my_window.config(cursor="wait")
def mouseFine():
    my_window.config(cursor="")
def cmdMode():
    global root_win_closed

    my_window.destroy()
    root_win_closed = True

    continueProg = True
    while continueProg:
        try:
            x = StockQuoter.StockQuote(input("Enter a stock symbol: "))
            x.getStock()
            print(x.stock_info_dataf)
            if((re.search("^[yY]+",input("Would you like a standard 3 month chart? y or n "))) != None):
                x.getChart()
        except:
            print("Something went wrong...")
        finally:
            if((re.search("^[yY]+",input("Do you wish to continue? y or n "))) == None):
                break
def closeWindow():
    global root_win_closed

    my_window.destroy()
    root_win_closed = True
def getStockInfo(event):
    try:
        x = StockQuoter.StockQuote(sym_entry.get())
        x.getStock()
        messagebox.showinfo("Stock Info",str(x.stock_info_dataf))
        x.chgChartDuration(combo_1.get())
        x.d50_ma = d50_state.get()
        x.d200_ma = d200_state.get()
        x.getChart()
    except:
        messagebox.showinfo("Information","Something went wrong...")
    finally:
        if(root_win_closed != True):
            sym_entry.delete(0,len(sym_entry.get()))     # Make sure that Tkinter root window is not destroyed first...
def getStockInfo2():                # b/c the first one is bound to an event
    try:
        x = StockQuoter.StockQuote(sym_entry.get())
        x.getStock()
        messagebox.showinfo("Stock Info",str(x.stock_info_dataf))
        x.chgChartDuration(combo_1.get())
        x.d50_ma = d50_state.get()
        x.d200_ma = d200_state.get()
        x.getChart()
    except:
        messagebox.showinfo("Information","Something went wrong...")
    finally:
        if(root_win_closed != True):
            sym_entry.delete(0,len(sym_entry.get()))     # Make sure that Tkinter root window is not destroyed first...

# setting up GUI*
my_window = tkinter.Tk()
my_window.title("Stock Program Study")
my_window.geometry("300x150")

root_menu = tkinter.Menu(my_window)
new_item_1 = tkinter.Menu(root_menu)
new_item_1.add_command(label="Exit",command=closeWindow)
root_menu.add_cascade(label="File", menu=new_item_1)
new_item_2 = tkinter.Menu(root_menu)
new_item_2.add_command(label="CMD", command=cmdMode)
root_menu.add_cascade(label="Mode", menu=new_item_2)
my_window.config(menu=root_menu)

label_1 = tkinter.Label(my_window,text="symbol")
label_1.grid(row=0,column=0,sticky="e")
sym_entry = tkinter.Entry(my_window,bg="beige",font="system",justify="center")
sym_entry.grid(row=0,column=1,sticky="w")
sym_entry.bind("<Return>",getStockInfo)
label_2 = tkinter.Label(my_window,text="chart")
label_2.grid(row=1,column=0,sticky="e")
combo_1 = ttk.Combobox()
combo_1['values'] = ["5d","1mo","3mo","6mo","YTD","MAX"]
combo_1.current(2)
combo_1.grid(row=1,column=1,sticky="w")
d50_state = tkinter.BooleanVar()
d200_state = tkinter.BooleanVar()
d50_ma = tkinter.Checkbutton(my_window,text="50d moving avg (fast signal)",var=d50_state)
d50_ma.grid(row=2,column=1,sticky="w")
d200_ma = tkinter.Checkbutton(my_window,text="200d moving avg (slow signal)",var=d200_state)
d200_ma.grid(row=3,column=1,sticky="w")
button_1 = tkinter.Button(my_window,text="go",command=getStockInfo2)
button_1.grid(row=4,column=1,sticky="w")

my_window.mainloop()
# End of GUI setup***/+

sys.exit(0)