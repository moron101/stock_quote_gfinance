import yfinance, re, sys, pandas
import matplotlib.pyplot as pl

# Reorganizes stock information into a more digestible format
#
# Usage:    x = yfinance.Ticket("IBM")
#           y = reOrgStockInt2List(x.info)
#
# Return:   a list containing the relevant stock info
def reOrgStockIn2List(passAlongDict):
    stock_info_list = []
    stock_info_list.append(["Company:",str(passAlongDict.get("longName"))])
    stock_info_list.append(["Employees:",str(passAlongDict.get("fullTimeEmployees"))])
    stock_info_list.append(["Today's Open:",str(passAlongDict.get("open"))])
    stock_info_list.append(["Previous Close:",str(passAlongDict.get("previousClose"))])
    stock_info_list.append(["Day's Range:",str(passAlongDict.get("dayLow")) + " - " + str(passAlongDict.get("dayHigh"))])
    stock_info_list.append(["Beta:",str(passAlongDict.get("beta"))])
    stock_info_list.append([" "," "])
    stock_info_list.append(["Ask:",str(passAlongDict.get("ask"))])
    stock_info_list.append(["Bid:",str(passAlongDict.get("bid"))])
    return stock_info_list

# Reorganizes stock information into a more digestible format (dataframe)
#
# Usage:    x = yfinance.Ticket("MSFT")
#           y = reOrgStockIn2Dataframe(x.info)
#
# Return:   a Panda dataframe of the relevant stock info
def reOrgStockIn2Dataframe(passAlongDict):
    #keyList = passAlongDict.keys()
    #valList = passAlongDict.values()
    # The above two lines gives everything returned by .info so the below is more selective
    keyList = ["Company","Employees","Today's Open","Previous Close","Day's Range","Beta","Ask","Bid"]
    valList = [passAlongDict.get("longName"),passAlongDict.get("fullTimeEmployees"),passAlongDict.get("open"),passAlongDict.get("previousClose"),str(passAlongDict.get("dayLow")) + " - " + str(passAlongDict.get("dayHigh")),passAlongDict.get("beta"),passAlongDict.get("ask"),passAlongDict.get("bid")]
    dataf = pandas.DataFrame(valList,index=keyList,columns=["-----------"])
    return dataf
# Plots a chart for the stock based on the historical dataframe return
def plotStockIn2Graph(passAlongDataframe):
    # date in Dataframe to list (but it's a dataframe timestamp datatype)
    x_axis = passAlongDataframe.index.tolist()
    # turn the timestamp datatype into string datatype in list
    x_axis = list(str(i)for i in x_axis)
    # turn list into a giant comma delimited string (so we can do some regex to extract pertinent data)
    my_str_amended = ",".join(x_axis)
    # extract only the date (and put back into list)
    x_axis = re.findall("[0-9]+[-][0-9]+[-][0-9]+",my_str_amended)
    # price in Dataframe to list
    y_axis = passAlongDataframe["Close"].tolist()
    # plot it
    pl.plot(x_axis, y_axis)
    # rotate the tick label
    pl.xticks(x_axis, rotation=45)
    # Pad margins so that markers don't get clipped by the axes
    pl.margins(0.1)
    # Tweak spacing to prevent clipping of tick-labels
    #pl.subplots_adjust(bottom=0.15)
    # Show only start, midpoint and endpoint of xticks (not to overcrowd)
    pl.xticks([x_axis[0],x_axis[int(len(x_axis)/2)],x_axis[len(x_axis)-1]])
    pl.ylabel("Price")
    pl.title("My Chart")
    pl.show()

continue_prog = True
while continue_prog:
    try:
        stock = yfinance.Ticker(input("Enter a stock symbol: "))

        # Used to display result after stock formatted into a list
        '''result = reOrgStockIn2List(stock.info)
        for i in result:
            print(i[0].ljust(20," "),i[1].ljust(20," "))'''
        # Used to display result after stock is formatted into a dataframe
        result = reOrgStockIn2Dataframe(stock.info)
        print(result)
        # Used to display graph of stock
        result = stock.history(period="3mo")
        plotStockIn2Graph(result)
    except:
        print("Something happened while retrieving your stock symbol...")
    finally:
        if((re.search("^[yY]+",input("Do you wish to continue? y or n "))) == None):
            break
sys.exit(0)