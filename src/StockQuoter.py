import yfinance, pandas, re
import matplotlib.pyplot as pl

class StockQuote():
    def __init__(self,stk_sym,chrt_dur=-90,d50_ma=False,d200_ma=False):
        self.stk_sym            = stk_sym.lower()
        self.chrt_dur           = chrt_dur
        self.d50_ma             = d50_ma
        self.d200_ma            = d200_ma
    def getStock(self):
        self.stock_result       = yfinance.Ticker(self.stk_sym)
        self.stock_result_dict  = self.stock_result.info
        self.stock_info_list    = self.reOrgStockIn2List(self.stock_result_dict)
        self.stock_info_dataf   = self.reOrgStockIn2Dataframe(self.stock_result.info)
    def chgChartDuration(self, chrt_dur):
        my_dict = {
            "5d"    : -5,
            "1mo"   : -30,
            "3mo"   : -90,
            "6mo"   : -180,
            "YTD"   : -365,
            "MAX"   : 0
        }
        self.chrt_dur = my_dict.get(chrt_dur)
    '''def getChart(self):              This is while I'm still learning about Dataframe
        self.stock_result_graph_dataf = self.stock_result.history(self.chrt_dur)
        # date in Dataframe to list (but it's a dataframe timestamp datatype)
        x_axis = self.stock_result_graph_dataf.index.tolist()
        # turn the timestamp datatype into string datatype in list
        x_axis = list(str(i)for i in x_axis)
        # turn list into a giant comma delimited string (so we can do some regex to extract pertinent data)
        my_str_amended = ",".join(x_axis)
        # extract only the date (and put back into list)
        x_axis = re.findall("[0-9]+[-][0-9]+[-][0-9]+",my_str_amended)
        # price in Dataframe to list
        y_axis = self.stock_result_graph_dataf["Close"].tolist()
        # plot it
        pl.plot(x_axis, y_axis)
        # rotate the tick label
        pl.xticks(x_axis, rotation=45)
        # Pad margins so that markers don't get clipped by the axes
        pl.margins(0.1)
        # Tweak spacing to prevent clipping of tick-labels
        #pl.subplots_adjust(bottom=0.15)
        # Show only start, midpoint and endpoint of xticks (not to overcrowd)
        pl.xticks([x_axis[0],x_axis[int(len(x_axis)/2)],x_axis[len(x_axis)-1]])
        pl.ylabel("Price")
        pl.title("My Chart")
        pl.show()'''
    def getChart(self):
        self.stock_result_graph_dataf_full = self.stock_result.history("MAX")                                               # used for calculation
        if(self.chrt_dur == 0):
            self.stock_result_graph_dataf_full["Close"].iloc[:,].plot(label="Close")
        else:
            self.stock_result_graph_dataf_full["Close"].iloc[self.chrt_dur:,].plot(label="Close")
        if(self.d50_ma == True):
            self.stock_result_graph_dataf_full["ma50"] = self.stock_result_graph_dataf_full["Close"].rolling(50).mean()     # Create new column
            if(self.chrt_dur == 0):
                self.stock_result_graph_dataf_full["ma50"].iloc[:,].plot(label="MA50")
            else:
                self.stock_result_graph_dataf_full["ma50"].iloc[self.chrt_dur:,].plot(label="MA50")                             #self.stock_result_graph_dataf["ma50"].loc[:].plot(label="MA50")
        if(self.d200_ma == True):
            self.stock_result_graph_dataf_full["ma200"] = self.stock_result_graph_dataf_full["Close"].rolling(200).mean()
            if(self.chrt_dur == 0):
                self.stock_result_graph_dataf_full["ma200"].iloc[:,].plot(label="MA200")
            else:
                self.stock_result_graph_dataf_full["ma200"].iloc[self.chrt_dur:,].plot(label="MA200")
        pl.legend()
        pl.title("Stock Chart")
        pl.show()
    # Reorganizes stock information into a more digestible format
    #
    # Usage:    x = yfinance.Ticket("IBM")
    #           y = reOrgStockInt2List(x.info)
    #
    # Return:   a list containing the relevant stock info
    def reOrgStockIn2List(self, passAlongDict):
        stock_info_list = []
        stock_info_list.append(["Company:",str(passAlongDict.get("longName"))])
        stock_info_list.append(["Employees:",str(passAlongDict.get("fullTimeEmployees"))])
        stock_info_list.append(["Today's Open:",str(passAlongDict.get("open"))])
        stock_info_list.append(["Previous Close:",str(passAlongDict.get("previousClose"))])
        stock_info_list.append(["Day's Range:",str(passAlongDict.get("dayLow")) + " - " + str(passAlongDict.get("dayHigh"))])
        stock_info_list.append(["Beta:",str(passAlongDict.get("beta"))])
        stock_info_list.append([" "," "])
        stock_info_list.append(["Ask:",str(passAlongDict.get("ask"))])
        stock_info_list.append(["Bid:",str(passAlongDict.get("bid"))])
        return stock_info_list
    # Reorganizes stock information into a more digestible format (dataframe)
    #
    # Usage:    x = yfinance.Ticket("MSFT")
    #           y = reOrgStockIn2Dataframe(x.info)
    #
    # Return:   a Panda dataframe of the relevant stock info
    def reOrgStockIn2Dataframe(self, passAlongDict):
        #keyList = passAlongDict.keys()
        #valList = passAlongDict.values()
        # The above two lines gives everything returned by .info so the below is more selective
        keyList = ["Company","Employees","Today's Open","Previous Close","Day's Range","Beta","Ask","Bid"]
        valList = [passAlongDict.get("longName"),passAlongDict.get("fullTimeEmployees"),passAlongDict.get("open"),passAlongDict.get("previousClose"),str(passAlongDict.get("dayLow")) + " - " + str(passAlongDict.get("dayHigh")),passAlongDict.get("beta"),passAlongDict.get("ask"),passAlongDict.get("bid")]
        dataf = pandas.DataFrame(valList,index=keyList,columns=["-----------"])
        return dataf